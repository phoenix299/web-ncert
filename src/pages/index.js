import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';
import HomepageFeatures from '../components/HomepageFeatures';
import Head from '@docusaurus/Head';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
      <div className="container2">
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className="container3">
        <iframe class="video" src="https://www.youtube-nocookie.com/embed/-fTFluOVkoY?controls=0&rel=0&autoplay=1&&loop=1&mute=1" title="TalentBrick Intro" frameborder="0" allow=" autoplay; picture-in-picture" allowfullscreen></iframe>
        <Link
            className="button button--secondary button--lg hmcl2 yts"
            to="https://www.youtube.com/channel/UC1rY1USiK4IvuwnCS92f90Q?sub_confirmation=1">
            Subscribe
          </Link>
          </div>
        </div>
        <div>
          <Link
            className="button button--secondary button--lg hmcl2"
            to="/intro">
            Start Learning Now ⏱
          </Link>
          <Link
            className="button button--secondary button--lg hmcl2 hmcl3"
            to="/blog">
            Read Blog
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Home`}
      description="Say hello to the Open-Source education model. Learning made easy without ADS and trackers, Clear concepts at a glance, and Get access to quality study materials only on TalentBrick.">
      <Head>
      <link rel="stylesheet" href="homepage.css"/>
      </Head>
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}
