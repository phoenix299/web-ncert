import React, {useLayoutEffect} from 'react';
import {useLocation} from '@docusaurus/router';
import Layout from '@theme-original/Layout';

function useLocationChangeTracker() {
  const {pathname} = useLocation();
  useLayoutEffect(() => {
    if (location.pathname.includes('mcq')) {
var labels = document.getElementsByTagName("label"); for (let a = 0; a < labels.length; a++)labels[a].addEventListener("click", addclass); function addclass(a) { a.target.classList.add("tb-wrng") }
function clearRadioGroup(GroupName)
{
  var ele = document.getElementsByTagName(GroupName);
    for(var i=0;i<ele.length;i++)
    ele[i].checked = false;
}
clearRadioGroup("input");
    }
  }, [pathname]);
}

function LayoutWrapper(props) {
  useLocationChangeTracker();
  return <Layout {...props} />;
}

export default LayoutWrapper;
