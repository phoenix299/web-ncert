---
title: How to crack NTSE with NCERTs
authors: omaditya
tags: [experience, strategy, subject-wise, Madhya Pradesh]
slug: crack-ntse
image: https://icdn.talentbrick.com/main/omdi-1.png
---

> The paper overall is very lengthy and one must learn not to take on their ego to tackle difficult questions and learn to skip the difficult ones.

I am Om Aditya Gupta and I am an NTSE scholar from the 2018 batch. I secured rank 12 in NTSE stage 1 from Madhya Pradesh state with 182 marks out of 195(5 ques were deleted) and 165 marks out of 200 in NTSE stage 2 . Today I am going to share my Ntse preparation story and provide you with some tips to clear the exam.

<!--truncate-->

## ABOUT
The National Talent Search Examination (NTSE) is a national-level scholarship program in India started in 1963. The NTSE exam is conducted by the National Council of Educational Research and Training (NCERT). It is a prestigious national-level exam that is conducted only for 10th standard students.  The objective of the exam is to give recognition to the talented students studying in class 10th.

The NTSE (National Talent Search Examination) examination is conducted every year at two levels – Stage I (State Level) and Stage II (National Level). NTSE Stage I is conducted by States and NTSE Stage II is conducted by NCERT. The paper is divided into two sections i.e. Mental Ability Test (MAT), and Scholastic Ability Test (SAT) each consisting of 100 questions. There is 2hrs devoted time for each MAT and SAT. SAT consists of 40 questions of Social Science, 20 questions of Maths, and 40 questions of Science.

## THE MOTIVATION
Preparing for a prestigious examination as such always requires dedication and motivation. To be honest for me the biggest motivation came from the scholarships and benefits of this exam. One gets Rs 1250/- monthly for the duration of 2 years in 11th and 12th and furthermore in the college years. Also, students who have cleared exams like NTSE and KVPY get the competitive edge in getting preference in foreign universities abroad as well in jobs.

## PREPARATION –
## Stage 1
First of all, I would like to add that it is very important to practice previous year question papers from at least the past 10 years for both stages 1 and 2 . This will help the aspirants to understand and predict the type and way of asking questions in the exam.

### MENTAL ABILITY
This is according to me by far the most important subject as it accounts for 100 marks out of the total 200. **MAT needs to be practiced every day**. Some important topics such as Series can account for as many as 15-18 ques in the exam. MAT can be practiced from any source as the syllabus is the same(it’s everything basically ) for every competitive exam.

### SOCIAL SCIENCE
Here the syllabus varies in various states as it is restricted to the syllabus of classes 9 and 10 of the STATE BOARD not CBSE (exception-Delhi). One needs to thoroughly revise and practice the subject as it accounts for 40 marks. It consists of 4 sections history, geography, civics, and economics where civics and economics face harsh discrimination and receive only 5 questions each whereas geography and history get 15 questions each. There is no requirement of any other reference books in this subject as the questions are entirely out of the state books except sometimes 1 or 2 questions may come from the CBSE syllabus.
 
The syllabus for the rest of the sections is the same as class 9th and 10th.

### MATHS
The syllabus sticks around class 9 and 10 with a bit of flavor of **number theory and sets from class 11**. Maths in stage 1 is generally on the easier side and a bit lengthy. Therefore One should have a good command over formulas and should be quick with the calculations. The candidates can easily score 20/20 in stage 1 by just practicing the topics every day and boosting their calculations. Some books which can turn out to be useful in the process-

1. [Maths Ncert class 9&10](https://amzn.to/3Fe3yHo) 
2. [RD SHARMA](https://amzn.to/30IxtbL)
3. [MTG Foundation Course](https://amzn.to/3q4Gog4)

### PHYSICS
Here as well the syllabus is the same as the classes 9 and 10 with particularly more focus on topics such as **Optics and Electromagnetism**. The questions are evenly distributed between the theoretical and numerical types. Physics can be easily tackled by sharpening the basics and revising all the formulas. Once again one needs to practice questions from all the topics thoroughly. For the same, I would like to recommend –

1. [HC VERMA](https://amzn.to/3Fc3mbK) (Theory)
2. [MTG Foundation Course](https://amzn.to/3q4Gog4)

### CHEMISTRY
There is no doubt chemistry has the easiest of questions in the paper, and therefore it is recommended that one should start their paper from chemistry.

Books to refer-
1. [Ncert 9 and 10](https://amzn.to/3GZpgzc)
2. [MTG Foundation Course](https://amzn.to/3q4Gog4) (for theory and Practice)

### BIOLOGY
There were around 13-14 questions in the biology section. Books –
1. [MTG Foundation Course](https://amzn.to/3q4Gog4)
2. [Ncert 9 and 10](https://amzn.to/3GZpgzc)

## Stage 2
NTSE stage 2 is conducted by the NCERT. The paper becomes very very lengthy and difficult as compared to stage 1. This may be verified from the fact as in stage 1 cutoff in some states may touch 190 whereas in stage 2 this cutoff remains close to 135. Once again MAT is the kingmaker. **Therefore one needs to practice MAT every single day**. The rest of the syllabus comes from NCERT textbooks of classes 9 and 10, this includes Social Science as well. The paper overall is very lengthy and one must learn not to take on their ego to tackle difficult questions and learn to skip the difficult ones. Overall in the exam TIME MANAGEMENT is the key. 

Also, I would like to share my experience from the stage 2 exam- in the MAT paper, I had wasted a lot of time in the first 20 questions and still had not gotten anywhere so I decided to start from the last in search of easy questions. And this turned out to be a very good move as there were much easier questions at the back at the end I scored 79 of the last 80 questions and just 11 from the first 20. Some of my friends couldn’t finish the paper cause they got stuck in the difficult 20 questions and couldn’t get to the easier ones. 

In the SAT part also as I have already mentioned the paper was too lengthy. Social science questions too were covered whole pages. Again I would suggest a time-saving tip, in the maths section, many of my friends were solving those hard and lengthy questions, they indeed got the correct answer but they did waste quite some time.  As for me, by chance, I looked at the options of some of the questions, and about 2 or 3 of them I was able to solve by just eliminating the options. For example, the options other than the correct answer did not have the correct unit as required by the question or so.  Such small tricks helped me score 75/100 in the SAT section. 

### Some important topics for Stage 2-

### Social Science- 
Climate, Drainage, Nationalism in India, forest and natural resources, revolutions in Europe(French and Russian), India’s size and location, Maps, Minerals, and energy resources, manufacturing industries, Nazism.

### Maths & Science-
You need to upgrade your knowledge from 9th and 10th up to the level of  11th and 12th. For eg- Medical students can study digestion, heart and circulation, excretion, genetics, and human health and disease in Biology subjects. Similarly Electromagnetism and Optics and Mechanics up to class 12th level.  In Maths, topics like Geometry(4-5 questions at least Number Theory, Algebra, and Trigonometry are important.

At last, I  would like to suggest the order of attempting the questions in the SAT part – Chemistry then Biology, Social science, Physics and then Maths. But everyone can devise their own plan of action to attempt the paper.
 






