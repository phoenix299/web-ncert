---
sidebar_position: 9
title: MCQ Questions Hydrogen Class 11 Inorganic Chemistry Chapter 9
description: Hydrogen Inorganic Chemistry Chapter 9 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/Hydrogen-MCQ-Talentbrick.png
sidebar_label: Chapter 9
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT** *Inorganic Chemistry* Hydrogen Class 11 Chapter 9, **Important MCQ Questions** for Jee Mains | NEET | Class 11.

Prepare these important Questions of Hydrogen in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams. 

### 1. Hydrogen is placed in which place in the periodic table?
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Group 18</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Separately in the Periodic Table</label><br />
    <input name="question1" type="radio" />
    <label>3. Group 17</label> <br />
    <input name="question1" type="radio" />
    <label>4. Group 1</label>
  </div><hr />

### 2. Catalyst used in the reaction of CO with steam is
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Copper Chromate</label><br />
    <input name="question2" type="radio" />
    <label>2. Cadmium Oxide</label><br />
    <input name="question2" type="radio" />
    <label>3. Nickel</label> <br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. Iron Chromate</label>
  </div><hr />

### 3. Internuclear distance between H2 and D2 is:
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. More for H2</label><br />
    <input name="question3" type="radio" />
    <label>2. More for D2</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. Equal for both</label> <br />
    <input name="question3" type="radio" />
    <label>4. Vary with Temperature</label>
  </div><hr />

### 4. Arrange the following in their increasing order of bond energy.
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. H2, F2, D2</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">2. F2, H2, D2</label><br />
    <input name="question4" type="radio" />
    <label>3. D2, F2, H2</label> <br />
    <input name="question4" type="radio" />
    <label>4. H2, D2, F2</label>
  </div><hr />

### 5. Presently most of the industrial dihydrogen gas is prepared from __
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Electrolysis of water</label><br />
    <input name="question5" type="radio" />
    <label>2. Coal</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">3. Petrochemicals</label> <br />
    <input name="question5" type="radio" />
    <label>4. Electrolysis of brine</label>
  </div><hr />

### 6. Single Bond energy is highest for which molecule?
  <div className="tb-mcq">
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">1. H-H Bond</label><br />
    <input name="question6" type="radio" />
    <label>2. C-C Bond</label><br />
    <input name="question6" type="radio" />
    <label>3. F-F Bond</label> <br />
    <input name="question6" type="radio" />
    <label>4. Cl-Cl Bond</label>
  </div><hr />

### 7. The ionization energy of hydrogen is similar to which of the following halogen?
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Fluorine</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">2. Chlorine</label><br />
    <input name="question7" type="radio" />
    <label>3. Iodine</label> <br />
    <input name="question7" type="radio" />
    <label>4. Bromine</label>
  </div><hr />

### 8. Choose the Correct Statement:
  <div className="tb-mcq">
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">1. H+ always exist in the associated form with other atoms or molecules</label><br />
    <input name="question8" type="radio" />
    <label>2. Harold C. Urey separated deuterium by chemical method</label><br />
    <input name="question8" type="radio" />
    <label>3. Deuterium is mostly formed in form of HDO</label> <br />
    <input name="question8" type="radio" />
    <label>4. Hydrogen is the second most abundant element on earth in its Combined State</label>
  </div><hr />

### 9. Lithium hydride reacts with aluminum chloride to give:
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. LiCl only</label><br />
    <input name="question9" type="radio" />
    <label>2. Aluminium Hydride</label><br />
    <input name="question9" type="radio" />
    <label>3. Lithium Aluminium Hydride</label> <br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. Both 1 and 3</label>
  </div><hr />

### 10. Heavy hydrogen is:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Tritium</label><br />
    <input name="question10" type="radio" />
    <label>2. Protium</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">3. Deuterium</label> <br />
    <input name="question10" type="radio" />
    <label>4. All of the above</label>
  </div><hr />

### 11. Which of the following statement is incorrect?
  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">1. Dihydrogen is used in the manufacture of Polyunsaturated vegetable oils</label><br />
    <input name="question11" type="radio" />
    <label>2. The largest single of dihydrogen is in the manufacturing of Ammonia</label><br />
    <input name="question11" type="radio" />
    <label>3. Many metals combine at high temperature with dihydrogen</label> <br />
    <input name="question11" type="radio" />
    <label>4. Hydroformylation of alkene leads to an increase in no. of carbon atoms in the chain</label>
  </div><hr />

### 12. Select the correct Statement:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. A mixture of CO and H2 is used for the preparation of methanoic acid</label><br />
    <input name="question12" type="radio" />
    <label>2. Methane and steam react to give carbon dioxide and hydrogen</label><br />
    <input name="question12" type="radio" />
    <label>3. Isotopes of hydrogen have almost the same physical property</label> <br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">4. Isotopes have a difference in their rate of reactions</label>
  </div><hr />

### 13. Which of the following statement is correct?<
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. All group 13 elements form electron-deficient compounds except Aluminum</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. Dihydrogen does not produce any Pollution</label><br />
    <input name="question13" type="radio" />
    <label>3. It produces greater energy per mole than gasoline and other fuels</label> <br />
    <input name="question13" type="radio" />
    <label>4. Lithium hydride reacts with Cl2 at moderate temperature to form Hydrochloric acid</label>
  </div><hr />

### 14. Which of the following makes the least contribution to the total world’s water supply?
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. River</label><br />
    <input name="question14" type="radio" />
    <label>2. Lakes</label><br />
    <input name="question14" type="radio" />
    <label>3. Soil moisture</label> <br />
    <input name="question14" type="radio" />
    <label>4. Atmospheric Water Vapor</label>
  </div><hr />

### 15. Incorrect with respect to metallic hydrides:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Metallic hydrides are hydrogen deficient.</label><br />
    <input name="question15" type="radio" />
    <label>2. Metallic hydride retain their metallic conductivity</label><br />
    <input name="question15" type="radio" />
    <label>3. Nickel and Actinium have different lattices from that of parent metal</label> <br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. These hydrides conduct as efficiently as their parent metals</label>
  </div><hr />

### 16. The human Body consists of how much water percentage?
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. 85%</label><br />
    <input name="question16" type="radio" />
    <label>2. 40%</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">3. 65%</label> <br />
    <input name="question16" type="radio" />
    <label>4. 95%</label>
  </div><hr />

 ### 17. Due to _____ with polar molecules, even covalent compounds like alcohol and carbohydrates dissolve in water. Fill in the blank:
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Dipole induced dipole interaction</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. H-Bonding</label><br />
    <input name="question17" type="radio" />
    <label>3. Van der Waal interactions</label> <br />
    <input name="question17" type="radio" />
    <label>4. London Force</label>
  </div><hr />

### 18. At Atmospheric pressure ice crystallizes in:
  <div className="tb-mcq">
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">1. Hexagonal form</label><br />
    <input name="question18" type="radio" />
    <label>2. Trigonal form</label><br />
    <input name="question18" type="radio" />
    <label>3. Tetragonal form</label> <br />
    <input name="question18" type="radio" />
    <label>4. Cubic form</label>
  </div><hr />

### 19. Magnesium ion forms which ppt on treatment with washing soda and calcium hydroxide respectively?
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. In both Mg(OH)2</label><br />
    <input name="question19" type="radio" />
    <label>2. In both MgCO3</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. MgCO3 and Mg(OH)2</label> <br />
    <input name="question19" type="radio" />
    <label>4. Mg(OH)2 and MgCO3</label>
  </div><hr />

### 20. In ice crystal structure distance between two oxygen atoms is about:
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Approximately equal to O-H bond length</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. More than the double of O-H bond length</label><br />
    <input name="question20" type="radio" />
    <label>3. Less than O-H bond length</label> <br />
    <input name="question20" type="radio" />
    <label>4. More than triple of O-H bond length</label>
  </div><hr />

### 21. Cation exchange resins contain ____ molecule.
  <div className="tb-mcq">
    <input name="question21" type="radio" />
    <label>1. Large, inorganic, water-insoluble</label><br />
    <input name="question21" type="radio" />
    <label>2. Small, inorganic, water-soluble</label><br />
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">3. Large, organic, water-insoluble</label> <br />
    <input name="question21" type="radio" />
    <label>4. Small, organic, water-soluble</label>
  </div><hr />

### 22. A commercially marketed sample of H₂O₂ is:
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>1. 2 V</label><br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">2. 10 V</label><br />
    <input name="question22" type="radio" />
    <label>3. 50 V</label> <br />
    <input name="question22" type="radio" />
    <label>4. 100 V</label>
  </div><hr />

### 23. Industrially Hydrogen peroxide is prepared by which method?
  <div className="tb-mcq">
    <input name="question23" type="radio" />
    <label>1. Electrolytic oxidation of acidified sulphate solutions</label><br />
    <input name="question23" type="radio" />
    <label>2. Exhaustive electrolysis of water</label><br />
    <input name="question23" type="radio" />
    <label>3. Acidifying barium peroxide</label> <br />
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">4. Auto oxidation of 2-alkyl anthraquinols</label>
  </div><hr />

### 24. BaCl2. xH2O is which type of hydrate and x is?
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. H-bonded, 4</label><br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">2. Interstitial,2</label><br />
    <input name="question24" type="radio" />
    <label>3. Interstitial,4</label> <br />
    <input name="question24" type="radio" />
    <label>4. Coordinated,2</label>
  </div><hr />

### 25. On Mass basis Dihydrogen can release about how much times more energy than petrol?
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>1. 4 times</label><br />
    <input name="question25" type="radio" />
    <label>2. 10 times</label><br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">3. 3 times</label> <br />
    <input name="question25" type="radio" />
    <label>4. 30 times</label>
  </div><hr />

### 26. The basic Principle of hydrogen economy is:
  <div className="tb-mcq">
    <input name="question26" type="radio" />
    <label>1. Transportation of gas in liquid form</label><br />
    <input name="question26" type="radio" />
    <label>2. Storage in liquid form</label><br />
    <input name="question26" type="radio" />
    <label>3. Transport in liquid form</label> <br />
    <input id="q26" name="question26" type="radio" />
    <label htmlFor="q26">4. All of these</label>
  </div><hr />

### 27. Arrange the following as per the order of energy released per gram:
  <div className="tb-mcq">
    <input id="q27" name="question27" type="radio" />
    <label htmlFor="q27">1. H₂ (g) &gt; CH4 &gt; LPG &gt; Octane</label><br />
    <input name="question27" type="radio" />
    <label>2. LPG &gt; CH4 &gt; Octane &gt; H₂ (g)</label><br />
    <input name="question27" type="radio" />
    <label>3. LPG &gt; H₂ (g) &gt; CH4 &gt; Octane</label> <br />
    <input name="question27" type="radio" />
    <label>4. H₂ (g) &gt; LPG &gt; CH4 &gt; Octane</label>
  </div><hr />

### 28. Arrange in order of increasing Electrical conductance:
  <div className="tb-mcq">
    <input name="question28" type="radio" />
    <label>1. BeH₂ &gt; TiH₂ &gt; CaH₂ </label><br />
    <input name="question28" type="radio" />
    <label>2. LPG &gt; CH4 &gt; Octane &gt; H₂ (g)</label><br />
    <input name="question28" type="radio" />
    <label>3. CaH₂ &gt; TiH₂ &gt; BeH₂</label> <br />
    <input id="q28" name="question28" type="radio" />
    <label htmlFor="q28">4. TiH₂ &gt; CaH₂ &gt; BeH₂</label>
  </div><hr />

### 29. H₂O₂ as an antiseptic is sold in the market as:
  <div className="tb-mcq">
    <input name="question29" type="radio" />
    <label>1. Per-carbonate</label><br />
    <input name="question29" type="radio" />
    <label>2. Cephalosporin</label><br />
    <input id="q29" name="question29" type="radio" />
    <label htmlFor="q29">3. Perhydrol</label> <br />
    <input name="question29" type="radio" />
    <label>4. Sodium perborate</label>
  </div><hr />

### 30. ___ is added as Stabilizer in H₂O₂.
  <div className="tb-mcq">
    <input name="question30" type="radio" />
    <label>1. Dust</label><br />
    <input id="q30" name="question30" type="radio" />
    <label htmlFor="q30">2. Urea</label><br />
    <input name="question30" type="radio" />
    <label>3. Zn Powder</label> <br />
    <input name="question30" type="radio" />
    <label>4. Carbamate</label>
  </div>