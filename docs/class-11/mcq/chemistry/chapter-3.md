---
sidebar_position: 3
title: MCQ Classification of Elements and Periodicity in Properties Class 11 Ch.3
description: Classification of Elements and Periodicity MCQ Questions Class 11, These are the latest questions to expect in NEET | JEE | School Exams | Competitive Exams.
image: https://1.bp.blogspot.com/-5ZTuFHZioOc/YIVBiyARyqI/AAAAAAAAAvs/sy-4pG_4ooUceJkIR6wBi1Rgq-gCIDBngCLcBGAsYHQ/s0/MCQ%2BClassification%2Bof%2BElements%2Band%2BPeriodicity%2Bin%2BProperties%2BClass%2B11%2BCh.3.png
sidebar_label: Chapter 3
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT** Inorganic Chemistry Classification of Elements and Periodicity in properties Class 11 Chemistry MCQ Questions for *Jee Mains | NEET | Class 11 Examination*.

Prepare these **Important Questions** of Classification of Elements and Periodicity in properties in **Inorganic Chemistry**, Latest questions to expect in Jee Mains | NEET | School Exams.

### 1. Scientist first to consider the idea of trends among properties of elements.
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Mendeleev</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Dobereiner</label><br />
    <input name="question1" type="radio" />
    <label>3. Seaborg</label><br />
    <input name="question1" type="radio" />
    <label>4. Moseley</label>
  </div><hr/>

### 2. Cylindrical table of elements was given by
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Dobereiner</label><br />
    <input name="question2" type="radio" />
    <label>2. Mendeleev</label><br />
    <input name="question2" type="radio" />
    <label>3. Lothar Meyer</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">4. A.E.B Chancourtois</label>
  </div><hr/>

### 3. Davy medal was awarded to
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Mendeleev</label><br />
    <input name="question3" type="radio" />
    <label>2. Meyer</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. Newland</label><br />
    <input name="question3" type="radio" />
    <label>4. A.E.B Chancourtois</label>
  </div><hr/>

### 4. Lothar Meyer plotted which of the following properties against atomic weight?
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Melting point</label><br />
    <input name="question4" type="radio" />
    <label>2. Boiling point</label><br />
    <input name="question4" type="radio" />
    <label>3. Atomic volume</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">4. All of the above</label>
  </div><hr/>

### 5. Who published periodic table for the first time ?
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Meyer</label><br />
    <input name="question5" type="radio" />
    <label>2. A.E.B Chancourtois</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">3. Mendeleev</label><br />
    <input name="question5" type="radio" />
    <label>4. Newland</label>
  </div><hr/>

### 6. Melting point of which of following was expected to be high?
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Eka-aluminium</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. Eka-silicon</label><br />
    <input name="question6" type="radio" />
    <label>3. Eka-scandium</label><br />
    <input name="question6" type="radio" />
    <label>4. Both 1 and 2</label>
  </div><hr/>

### 7. Which of the following belong to group VII?
A. Manganese  
B. Iodine  
C. Chlorine
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. Only B and C</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">2. A, B, C</label><br />
    <input name="question7" type="radio" />
    <label>3. Only C</label><br />
    <input name="question7" type="radio" />
    <label>4. Only B</label>
  </div><hr/>

### 8. Osmium and Platinum belong which group under Mendeleev periodic table?
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. VI</label><br />
    <input name="question8" type="radio" />
    <label>2. V</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">3. VIII</label><br />
    <input name="question8" type="radio" />
    <label>4. VII</label>
  </div><hr/>

### 9. Which of the following is correct according to IUPAC convection?
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Zn – group IIB</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">2. Pd – group 10</label><br />
    <input name="question9" type="radio" />
    <label>3. Co – group VIA</label><br />
    <input name="question9" type="radio" />
    <label>4. Ni-family 8</label>
  </div><hr/>

### 10. Most widely used electronegativity scale is:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Mulliken Jaffe scale</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">2. Pauling scale</label><br />
    <input name="question10" type="radio" />
    <label>3. Allred Rochow scale</label><br />
    <input name="question10" type="radio" />
    <label>4. Both 1 and 3</label>
  </div><hr/>

### 11. Transuranium series begin from:
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Uranium</label><br />
    <input name="question11" type="radio" />
    <label>2. Rutherfordium</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">3. Neptunium</label><br />
    <input name="question11" type="radio" />
    <label>4. Lawrencium</label>
  </div><hr/>

### 12. Arrange the following in increasing order of ionization energy?
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. He&lt;Li&lt;Li⁺&lt;He⁺</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">2. Li&lt;He&lt;He⁺&lt;Li⁺</label><br />
    <input name="question12" type="radio" />
    <label>3. Li&lt;Li⁺&lt;He&lt;He⁺</label><br />
    <input name="question12" type="radio" />
    <label>4. Li&lt;He&lt;Li⁺&lt;He⁺</label>
  </div><hr/>

### 13. Non-metals in periodic table comprise total number
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Less than 78</label><br />
    <input name="question13" type="radio" />
    <label>2. More than 78</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">3. Less than 20</label><br />
    <input name="question13" type="radio" />
    <label>4. More than 20</label>
  </div><hr/>

### 14. Choose the incorrect statement.
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. Lithium is more electronegative as compared to Astatine</label><br />
    <input name="question14" type="radio" />
    <label>2. Oxides of elements in center are amphoteric or neutral</label><br />
    <input name="question14" type="radio" />
    <label>3. Highest positive electron gain enthalpy is of neon</label><br />
    <input name="question14" type="radio" />
    <label>4. Chemical reactivity is highest at two extremes of periodic table</label>
  </div><hr/>

### 15. Group having metal , non-metal, liquid as well as gas at room temperature is ___
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Group 16</label><br />
    <input name="question15" type="radio" />
    <label>2. Group 18</label><br />
    <input name="question15" type="radio" />
    <label>3. Group 3</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. Group 17</label>
  </div><hr/>

### 16. Pitch blende is ore of ___
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Mercury</label><br />
    <input name="question16" type="radio" />
    <label>2. Cerium</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">3. Uranium</label><br />
    <input name="question16" type="radio" />
    <label>4. Neptunium</label>
  </div><hr/>

### 17. The scientist who is generally credited with development of modern periodic table is
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Moseley</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. Mendeleev</label><br />
    <input name="question17" type="radio" />
    <label>3. Meyer</label><br />
    <input name="question17" type="radio" />
    <label>4. Newland</label>
  </div><hr/>

### 18. The symbol Uut is used for which element
  <div className="tb-mcq">
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">1. Nihonium</label><br />
    <input name="question18" type="radio" />
    <label>2. Seaborgium</label><br />
    <input name="question18" type="radio" />
    <label>3. Flerovium</label><br />
    <input name="question18" type="radio" />
    <label>4. Neptunium</label>
  </div><hr/>

### 19. State true or false:
A. Soviets named element 104 as Kurchatovium.  
B. Metals comprise less than 78% of all known elements.
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. T T</label><br />
    <input name="question19" type="radio" />
    <label>2. F T</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. T F</label><br />
    <input name="question19" type="radio" />
    <label>4. F T</label>
  </div><hr/>

### 20. Reactivity increases to maximum in the group
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>a. 1</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">b. 17</label><br />
    <input name="question20" type="radio" />
    <label>c. 13</label><br />
    <input name="question20" type="radio" />
    <label>d. 16</label>
  </div>