---
sidebar_position: 12
title: MCQ Organic Chemistry Some Basic Principles and Techniques Ch.12
description: Some Basic Principles and Techniques Organic Chemistry Chapter 12 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Organic-Chemistry-Some-Basic-Principles-and-Techniques-Ch.12.png
sidebar_label: Chapter 12
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT** Chemistry Class 11 MCQ Practice Questions Organic Chemistry Some Basic Principles and Techniques Chapter 12 for JEE | NEET | Class 11.

Prepare these **important** [MCQ Questions of Chemistry Class 11](/class-11/mcq/chemistry) Some Basic Principles and Techniques in Chemistry, Latest questions to expect in NEET | JEE | School Exams.

### 1. Incorrect match among the following.
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. Kolbe : Acetic acid</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. Berzelius : Benzene</label><br />
    <input name="question1" type="radio" />
    <label>3. Berthelot : Methane</label><br />
    <input name="question1" type="radio" />
    <label>4. Wohler : Urea</label></div><hr />

### 2. Identify a and b in the given diagram:
![Identify a](https://icdn.talentbrick.com/mcq/Qchem-a.png) ![Identify b](https://icdn.talentbrick.com/mcq/Qchem-b.png)
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Both A and B are aromatic</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">2. A is aromatic and B is non aromatic</label><br />
    <input name="question2" type="radio" />
    <label>3. Both A and B are non aromatic</label><br />
    <input name="question2" type="radio" />
    <label>4. A is anti-aromatic and B is aromatic</label></div><hr />

### 3. Molecular formula for Icosane is:
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. C₁₁H₂₄</label><br />
    <input name="question3" type="radio" />
    <label>2. C₃₀H₆₂</label><br />
    <input name="question3" type="radio" />
    <label>3. C₁₃H₂₈</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">4. C₂₀H₄₂</label></div><hr />

### 4. State true or false
A. Unsaturated hydrocarbons contain at least one carbon carbon double bond or triple bond.  
B. Nitrometer is used in Kjeldahl’s method.  
C. Percentage of phosphorous using ammonium phophomolybdate is given by 31 x m₁ x 100 %/1877 x m.
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. F, F, T</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">2. T, F, T</label><br />
    <input name="question4" type="radio" />
    <label>3. T, T, T</label><br />
    <input name="question4" type="radio" />
    <label>4. F, T, F</label></div><hr />

### 5. Me₂S when undergoes heterolytic cleavage forms:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Carbonium ion</label><br />
    <input name="question5" type="radio" />
    <label>2. Carbanion</label><br />
    <input name="question5" type="radio" />
    <label>3. Carbocation</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">4. Both 1 and 3</label></div><hr />

### 6. Which effect is also called as Polarisability effect?
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Inductive effect</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">2. Electromeric effect</label><br />
    <input name="question6" type="radio" />
    <label>3. Resonance effect</label><br />
    <input name="question6" type="radio" />
    <label>4. Hyperconjugation effect</label></div><hr />

### 7. Which of the following compounds have all C-C bond length equal?
A. Phenol  
B. Benzene  
C. Toluene  
D. Para ethoxy phenol
  <div className="tb-mcq">
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">1. Only B</label><br />
    <input name="question7" type="radio" />
    <label>2. A, B, C only</label><br />
    <input name="question7" type="radio" />
    <label>3. A,B,C,D</label><br />
    <input name="question7" type="radio" />
    <label>4. Only A and B</label></div><hr />

### 8. CuSO₄ in Kjeldahl method acts as:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Oxidising agent</label><br />
    <input name="question8" type="radio" />
    <label>2. Reducing agent</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">3. Catalyst</label><br />
    <input name="question8" type="radio" />
    <label>4. Hydrolysis agent</label></div><hr />

### 9. Which of the following groups have enhanced acidic or basic character due to identical resonating structures?
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Carboxylic acid</label><br />
    <input name="question9" type="radio" />
    <label>2. Sulphonic acids</label><br />
    <input name="question9" type="radio" />
    <label>3. Guanidine</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">4. All of the above</label></div><hr />

### 10. Which of following is temporary effect?
  <div className="tb-mcq">
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">1. Electromeric effect</label><br />
    <input name="question10" type="radio" />
    <label>2. Resonance</label><br />
    <input name="question10" type="radio" />
    <label>3. Hyperconjugation</label><br />
    <input name="question10" type="radio" />
    <label>4. Both 1 and 3</label></div><hr />

### 11. The best and latest technique for isolation and separation of organic compounds is:
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Crystallization</label><br />
    <input name="question11" type="radio" />
    <label>2. Sublimation</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">3. Chromatography</label><br />
    <input name="question11" type="radio" />
    <label>4. Fractional distillation</label></div><hr />

### 12. Coordination number of blood red complex formed due to Thiocyanate ion is
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>a. 2</label><br />
    <input name="question12" type="radio" />
    <label>b. 1</label><br />
    <input name="question12" type="radio" />
    <label>c. 4</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">d. 6</label></div><hr />

### 13. Which of the following method is used for separation of glycerol from spent- lye?
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Steam distillation</label><br />
    <input name="question13" type="radio" />
    <label>2. Distillation under reduced pressure</label><br />
    <input name="question13" type="radio" />
    <label>3. Fractional distillation</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. Both 2 and 3</label></div><hr />

### 14. Commonly used adsorbent/s used for chromatography are
  <div className="tb-mcq">
    <input name="question14" type="radio" />
    <label>1. Acetonitrile</label><br />
    <input name="question14" type="radio" />
    <label>2. Alumina</label><br />
    <input name="question14" type="radio" />
    <label>3. Silica gel</label><br />
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">4. Both 2 and 3</label></div><hr />

### 15. Kjeldahl method is not applicable for
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Pyridine</label><br />
    <input name="question15" type="radio" />
    <label>2. Methylamine</label><br />
    <input name="question15" type="radio" />
    <label>3. Quinoline</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. Both 1 and 3</label></div><hr />

### 16. AgF is not detected by Lassaigne’s test beacuse:
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Fluoride gets oxidised to form fluorine gas.</label><br />
    <input name="question16" type="radio" />
    <label>2. Fluoride ion forms precipitate with sodium extract.</label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">3. AgF is soluble in water.</label><br />
    <input name="question16" type="radio" />
    <label>4. AgF is not stable and hence not formed.</label></div><hr />

### 17. CHN elemental analyzer can detect small quantities upto:
  <div className="tb-mcq">
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">a. 1-3 mg</label><br />
    <input name="question17" type="radio" />
    <label>b. 10-30 mg</label><br />
    <input name="question17" type="radio" />
    <label>c. 1-3 gm</label><br />
    <input name="question17" type="radio" />
    <label>d. 0.1-0.3 mg</label></div><hr />

### 18. Percentage of oxygen in organic compound is usually found by
  <div className="tb-mcq">
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">1. Difference between the total percentage composition and sum of all percentages of all other elements.</label><br />
    <input name="question18" type="radio" />
    <label>2. By converting oxygen into CO and then estimating CO using iodine pentoxide.</label><br />
    <input name="question18" type="radio" />
    <label>3. By converting oxygen into iodine pentoxide and then estimating iodine pentoxide using CO.</label><br />
    <input name="question18" type="radio" />
    <label>4. Using coke to directly convert oxygen into CO₂.</label></div><hr />

### 19. Which acid can be used for acidification of sodium extract for testing sulphur by lead acetate test?
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. Sulphuric acid</label><br />
    <input name="question19" type="radio" />
    <label>2. Nitric acid</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. Acetic acid</label><br />
    <input name="question19" type="radio" />
    <label>4. All of these</label></div><hr />

### 20. Nitric acid converts cyanide and sulphide ions into which gases during test for halogens?
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. CO₂ and H₂S</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. HCN and H₂S</label><br />
    <input name="question20" type="radio" />
    <label>3. CO₂ and SO₂</label><br />
    <input name="question20" type="radio" />
    <label>4. CO and SO₂</label></div>