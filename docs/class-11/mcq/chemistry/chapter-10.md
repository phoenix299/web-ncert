---
sidebar_position: 10
title: MCQ Questions S Block Elements Class 11 Chemistry Chapter 10
description: S Block Elements Chemistry Chapter 10 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Questions-S-Block-Elements-Class-11-Chemistry-Chapter-10.png
sidebar_label: Chapter 10
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

MCQ Quiz s-Block Elements [Chemistry Class 11 MCQ Questions](/class-11/mcq/chemistry) for NEET |  School Exams | Class 11

Prepare these important MCQ Questions of s-Block Elements, These are Latest questions to expect in Jee | NEET | School Exams.

### 1. MgCl₂.6H₂O on strong heating gives:
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. [Mg(OH)₆]Cl₂</label><br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">2. MgO</label><br />
    <input name="question1" type="radio" />
    <label>3. Mg(OH)₂</label><br />
    <input name="question1" type="radio" />
    <label>4. MgCl₂ anhydrous</label></div><hr />

### 2. With respect to abundance in earth crust rank of magnesium and calcium are respectively
  <div className="tb-mcq">
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">a. 6,5</label><br />
    <input name="question2" type="radio" />
    <label>b. 5,6</label><br />
    <input name="question2" type="radio" />
    <label>c. 4,6</label><br />
    <input name="question2" type="radio" />
    <label>d. 8,10</label></div><hr />

### 3. Choose the incorrect statement:
  <div className="tb-mcq">
    <input name="question3" type="radio" />
    <label>1. Monovalent ions of alkali metals are never found in free state in nature.</label><br />
    <input name="question3" type="radio" />
    <label>2. Alkali metals and their salts impart characteristic colour to oxidizing flame.</label><br />
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">3. Water of crystallisation in Lithium chloride are 3.</label><br />
    <input name="question3" type="radio" />
    <label>4. Alkali metal have largest size in a particular period.</label></div><hr />

### 4. White metal is alloy of:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. Na and Pb</label><br />
    <input name="question4" type="radio" />
    <label>2. Li and Na</label><br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">3. Li and Pb</label><br />
    <input name="question4" type="radio" />
    <label>4. Mg and Pb</label></div><hr />

### 5. Alloy used to make anti knock additive is:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Li/Pb</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">2. Na/Pb</label><br />
    <input name="question5" type="radio" />
    <label>3. Mg/Pb</label><br />
    <input name="question5" type="radio" />
    <label>4. Ba/Pb</label></div><hr />

### 6. Choose the incorrect statement:
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Alkali metals form salts with all oxoacids.</label><br />
    <input name="question6" type="radio" />
    <label>2. The superoxide ion is stable only in presence of large cations.</label><br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">3. Silver (Ag) is better reducing agent than iodide ion (I⁻).</label><br />
    <input name="question6" type="radio" />
    <label>4. In most cases hydrogencarbonates are highly stable to heat.</label></div><hr />

### 7. Identify the correct statement with respect to lithium.
  <div className="tb-mcq">
    <input name="question7" type="radio" />
    <label>1. It forms ethynide on reaction with ethyne.</label><br />
    <input name="question7" type="radio" />
    <label>2. It is most reactive among all alkali metals.</label><br />
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">3. It is strongest reducing agent.</label><br />
    <input name="question7" type="radio" />
    <label>4. Its hydride is covalent in nature.</label></div><hr />

### 8. Both Mg and Li are:
  <div className="tb-mcq">
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">1. Harder and lighter than other elements in respective groups.</label><br />
    <input name="question8" type="radio" />
    <label>2. Softer and lighter than other elements in respective groups.</label><br />
    <input name="question8" type="radio" />
    <label>3. Harder and heavier than other elements in respective groups.</label><br />
    <input name="question8" type="radio" />
    <label>4. Softer and dense than other elements in respective groups.</label></div><hr />

### 9. How many of the following sodium compounds are used in textile industry?
| NaCl, NaOH, Na₂CO₃, NaHCO₃ |
|-----------------------------|
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>a. 4</label><br />
    <input name="question9" type="radio" />
    <label>b. 1</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">c. 2</label><br />
    <input name="question9" type="radio" />
    <label>d. 3</label></div><hr />

### 10. Compound of sodium used for mercerizing of cotton fabrics and for preparation of pure fat and oils.
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. NaCl</label><br />
    <input name="question10" type="radio" />
    <label>2. Na₂CO₃</label><br />
    <input name="question10" type="radio" />
    <label>3. NaHCO₃</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">4. NaOH</label></div><hr />

### 11. Mark the correct order of amount of elements found in human body.
  <div className="tb-mcq">
    <input name="question11" type="radio" />
    <label>1. Ca&gt;Na&gt;Mg&gt;Cu&gt;Fe</label><br />
    <input name="question11" type="radio" />
    <label>2. Ca&gt;Mg&gt;Na&gt;Cu&gt;Fe</label><br />
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">3. Ca&gt;Na&gt;Mg&gt;Fe&gt;Cu</label><br />
    <input name="question11" type="radio" />
    <label>4. Ca&gt;Na&gt;Fe&gt;Mg&gt;Cu</label></div><hr />

### 12. Choose the incorrect statement:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Sodium hydroxide reacts with CO₂ in the atmosphere to form Na₂CO₃.</label><br />
    <input name="question12" type="radio" />
    <label>2. Calcium and magnesium chloride are more soluble than NaCl.</label><br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">3. Washing soda turns to soda ash by heating at 373K.</label><br />
    <input name="question12" type="radio" />
    <label>4. Beryllium and magnesium appear to be somewhat greyish.</label></div><hr />

### 13. Odd one out with respect to rock salt structure.
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. MgO</label><br />
    <input name="question13" type="radio" />
    <label>2. BaO</label><br />
    <input name="question13" type="radio" />
    <label>3. CaO</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">4. BeO</label></div><hr />

### 14. Element used for making windows of X-ray tubes.
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. Be</label><br />
    <input name="question14" type="radio" />
    <label>2. Ba</label><br />
    <input name="question14" type="radio" />
    <label>3. Ca</label><br />
    <input name="question14" type="radio" />
    <label>4. Mg</label></div><hr />

### 15. Select the incorrect match with respect to elements and their uses.
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. Ra – Radiotherapy</label><br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">2. Be – Remove air from vaccum tubes</label><br />
    <input name="question15" type="radio" />
    <label>3. Ca – Extraction of metals</label><br />
    <input name="question15" type="radio" />
    <label>4. Mg – Incendiary bombs</label></div><hr />

### 16. Correct order of solubility of nitrates.
  <div className="tb-mcq">
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">1. Be(NO₃)₂&gt;Mg(NO₃)₂&gt;Ca(NO₃)₂&gt;Sr(NO₃)₂</label><br />
    <input name="question16" type="radio" />
    <label>2. Be(NO₃)₂&lt;Mg(NO₃)₂&lt;Ca(NO₃)₂&lt;Sr(NO₃)₂</label><br />
    <input name="question16" type="radio" />
    <label>3. Be(NO₃)₂&lt;Mg(NO₃)₂&gt;Ca(NO₃)₂&lt;Sr(NO&lt;₃)₂</label><br />
    <input name="question16" type="radio" />
    <label>4. Be(NO₃)₂&gt;Mg(NO₃)₂&gt;Ca(NO₃)₂&lt;Sr(NO₃)₂</label></div><hr />

### 17.Compound of alkaline earth metal that is/are stable to heat.
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Nitrates</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. Sulphates</label><br />
    <input name="question17" type="radio" />
    <label>3. Carbonates</label><br />
    <input name="question17" type="radio" />
    <label>4. All of these</label></div><hr />

### 18. From solution of which metal in ammonia, [M(NH₃)₆]⁺² can be recovered.
  <div className="tb-mcq">
    <input name="question18" type="radio" />
    <label>1. Na&nbsp;</label><br />
    <input name="question18" type="radio" />
    <label>2. Be</label><br />
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">3. Mg</label><br />
    <input name="question18" type="radio" />
    <label>4. Li</label></div><hr />

### 19. Sodium hydrogencarbonate and calcium hydroxide are respectively:
  <div className="tb-mcq">
    <input name="question19" type="radio" />
    <label>1. Disinfectant, Antiseptic</label><br />
    <input name="question19" type="radio" />
    <label>2. Both Disinfectant</label><br />
    <input id="q19" name="question19" type="radio" />
    <label htmlFor="q19">3. Antiseptic, Disinfectant</label><br />
    <input name="question19" type="radio" />
    <label>4. Both Antiseptic</label></div><hr />

### 20. Choose the correct statement:
A. Sodium-potassium pump consumes more than one third of the ATP used by resting animal.  
B. CaCO₃ is extensively used in manufacture of high quality paper.
  <div className="tb-mcq">
    <input name="question20" type="radio" />
    <label>1. Both A and B are incorrect</label><br />
    <input id="q20" name="question20" type="radio" />
    <label htmlFor="q20">2. Both A and B are correct</label><br />
    <input name="question20" type="radio" />
    <label>3. Only A is correct</label><br />
    <input name="question20" type="radio" />
    <label>4. Only B is correct</label></div><hr />

### 21. Which of the following is cheapest form of alkali?
  <div className="tb-mcq">
    <input id="q21" name="question21" type="radio" />
    <label htmlFor="q21">1. CaO</label><br />
    <input name="question21" type="radio" />
    <label>2. BaO</label><br />
    <input name="question21" type="radio" />
    <label>3. NaOH</label><br />
    <input name="question21" type="radio" />
    <label>4. CaCO₃</label></div><hr />

### 22. Choose the correct one:
  <div className="tb-mcq">
    <input name="question22" type="radio" />
    <label>1. NaOH&gt;NaCl (melting point).</label><br />
    <input id="q22" name="question22" type="radio" />
    <label htmlFor="q22">2. E⁰ for M²⁺ + 2e-→M(s) for Ca, Sr, Ba is nearly constant.</label><br />
    <input name="question22" type="radio" />
    <label>3. Potassium found to be more useful than sodium.</label><br />
    <input name="question22" type="radio" />
    <label>4. LiCl is insoluble in water but soluble in acetone.</label></div><hr />

### 23. State true or false.
A. Fire caused by saline hydride can be extinguished by CO₂.  
B. Calcium hydroxide is used for purification of sugar.  
C. CaO on exposure to atmosphere absorbs CO₂ and moisture.
  <div className="tb-mcq">
    <input name="question23" type="radio" />
    <label>1. T, F, T</label><br />
    <input name="question23" type="radio" />
    <label>2. F, F, T</label><br />
    <input id="q23" name="question23" type="radio" />
    <label htmlFor="q23">3. F, T, T</label><br />
    <input name="question23" type="radio" />
    <label>4. F, F, F</label></div><hr />

### 24. Beryllium chloride has which hybridization at 1200K?
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. sp²</label><br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">2. sp</label><br />
    <input name="question24" type="radio" />
    <label>3. sp³</label><br />
    <input name="question24" type="radio" />
    <label>4. Unhybridized</label></div><hr />

### 25. Upto what extent bone is solubilied and redeposited daily?
  <div className="tb-mcq">
    <input name="question25" type="radio" />
    <label>1. 30 mg</label><br />
    <input id="q25" name="question25" type="radio" />
    <label htmlFor="q25">2. 400 mg</label><br />
    <input name="question25" type="radio" />
    <label>3. 40 gm</label><br />
    <input name="question25" type="radio" />
    <label>4. 800 gm</label></div><hr />

### 26. Portland cement has which of the following ingredients as major component?
  <div className="tb-mcq">
    <input name="question26" type="radio" />
    <label>1. Gypsum</label><br />
    <input id="q26" name="question26" type="radio" />
    <label htmlFor="q26">2. Tricalcium silicate</label><br />
    <input name="question26" type="radio" />
    <label>3. Dicalcium silicate</label><br />
    <input name="question26" type="radio" />
    <label>4. Tricalcium aluminate</label></div><hr />

### 27. Which element’s oxide is not used in cement preparation?
  <div className="tb-mcq">
    <input name="question27" type="radio" />
    <label>1. Sulphur</label><br />
    <input name="question27" type="radio" />
    <label>2. Iron</label><br />
    <input name="question27" type="radio" />
    <label>3. Magnesium</label><br />
    <input id="q27" name="question27" type="radio" />
    <label htmlFor="q27">4. Copper</label></div>

### 28. The atomic size of Potassium is bigger than that of
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Barium</label><br />
    <input name="question10" type="radio" />
    <label>2. Calcium</label><br />
    <input name="question10" type="radio" />
    <label>3. Strontium</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">4. All of these</label></div><hr />

### 29. Which element’s hydride is synthesised above 1000K temperature?
  <div className="tb-mcq">
    <input name="question24" type="radio" />
    <label>1. Cs</label><br />
    <input id="q24" name="question24" type="radio" />
    <label htmlFor="q24">2. Li</label><br />
    <input name="question24" type="radio" />
    <label>3. Na</label><br />
    <input name="question24" type="radio" />
    <label>4. K</label></div><hr />


