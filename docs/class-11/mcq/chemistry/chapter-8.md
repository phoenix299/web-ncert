---
sidebar_position: 8
title: MCQ Questions Redox Reactions Inorganic Chemistry Class 11 Chapter 8
description: Redox Reactions Inorganic Chemistry Chapter 8 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Questions-Redox-Reactions-Inorganic-Chemistry-Class-11-Chapter-8.png
sidebar_label: Chapter 8
---

<head>
<link rel="stylesheet" href="https://icdn.talentbrick.com/css/mcq-ques.css" />
</head>

**NCERT** *Inorganic Chemistry* Redox Reactions Class 11 Chapter 8, **Important MCQ Questions** for Jee Mains | NEET | Class 11.

Prepare these important Questions of Redox Reactions in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams. 

### 1. Which of the following is correct Stock notation?
  <div className="tb-mcq">
    <input name="question1" type="radio" />
    <label>1. H(I)AuCl₃</label><br />
    <input name="question1" type="radio" />
    <label>2. HAu(I)Cl₃</label><br />
    <input name="question1" type="radio" />
    <label>3. HAuCl₃(III)</label> <br />
    <input id="q1" name="question1" type="radio" />
    <label htmlFor="q1">4. HAu(III)Cl₃</label>
  </div><hr/>

### 2. Indicator used in the titration of Potassium dichromate is:
  <div className="tb-mcq">
    <input name="question2" type="radio" />
    <label>1. Starch solution</label><br />
    <input name="question2" type="radio" />
    <label>2. Hydrogen peroxide</label><br />
    <input id="q2" name="question2" type="radio" />
    <label htmlFor="q2">3. Diphenylamine</label> <br />
    <input name="question2" type="radio" />
    <label>4. Biphenyl</label>
  </div><hr/>

### 3. Layer test is used for the detection of:
  <div className="tb-mcq">
    <input id="q3" name="question3" type="radio" />
    <label htmlFor="q3">1. Bromide ion</label><br />
    <input name="question3" type="radio" />
    <label>2. Sulfide ion</label><br />
    <input name="question3" type="radio" />
    <label>3. Chloride ion</label> <br />
    <input name="question3" type="radio" />
    <label>4. Fluoride ion</label>
  </div><hr/>

### 4. The substance to be used as a primary standard should satisfy which of the following requirements:
  <div className="tb-mcq">
    <input name="question4" type="radio" />
    <label>1. It should not be hygroscopic.</label><br />
    <input name="question4" type="radio" />
    <label>2. It should have a high relative molecular mass.</label><br />
    <input name="question4" type="radio" />
    <label>3. Its reaction with another substance should be instantaneous and stoichiometric.</label> <br />
    <input id="q4" name="question4" type="radio" />
    <label htmlFor="q4">4. All of these.</label>
  </div><hr/>

### 5. Phenolphthalein is a:
  <div className="tb-mcq">
    <input name="question5" type="radio" />
    <label>1. Weak base</label><br />
    <input id="q5" name="question5" type="radio" />
    <label htmlFor="q5">2. Weak acid</label><br />
    <input name="question5" type="radio" />
    <label>3. Amphiprotic ion</label> <br />
    <input name="question5" type="radio" />
    <label>4. Strong acid</label>
  </div><hr/>

### 6. ClO⁻ disproportionate to give:
  <div className="tb-mcq">
    <input name="question6" type="radio" />
    <label>1. Cl⁻ and ClO⁻ </label><br />
    <input name="question6" type="radio" />
    <label>2. Cl₂ and ClO₄⁻</label><br />
    <input name="question6" type="radio" />
    <label>3. Cl⁻ and ClO₂⁻ </label> <br />
    <input id="q6" name="question6" type="radio" />
    <label htmlFor="q6">4. Cl⁻ and ClO₃⁻</label>
  </div><hr/>

### 7. In the reaction of metallic cobalt and nickel sulphate solution, the equilibrium test reveals the presence of:
  <div className="tb-mcq">
    <input id="q7" name="question7" type="radio" />
    <label htmlFor="q7">a. Ni⁺² and Co⁺²</label><br />
    <input name="question7" type="radio" />
    <label>b. Ni⁺² only</label><br />
    <input name="question7" type="radio" />
    <label>c. Co⁺² only</label> <br />
    <input name="question7" type="radio" />
    <label>d. Can’t be predicted</label>
  </div><hr/>

### 8. The non-metal displacement redox reaction include:
  <div className="tb-mcq">
    <input name="question8" type="radio" />
    <label>1. Hydrogen and oxygen displacement.</label><br />
    <input name="question8" type="radio" />
    <label>2. Rarely hydrogen displacement, no oxygen displacement.</label><br />
    <input id="q8" name="question8" type="radio" />
    <label htmlFor="q8">3. Hydrogen displacement and rarely oxygen displacement.</label> <br />
    <input name="question8" type="radio" />
    <label>4. No oxygen displacement and rarely hydrogen displacement.</label>
  </div><hr/>

### 9. AgF₂ is a:
  <div className="tb-mcq">
    <input name="question9" type="radio" />
    <label>1. Very strong reducing agent</label><br />
    <input id="q9" name="question9" type="radio" />
    <label htmlFor="q9">2. Very strong oxidising agent</label><br />
    <input name="question9" type="radio" />
    <label>3. Weakly reducing agent</label> <br />
    <input name="question9" type="radio" />
    <label>4. Weakly oxidising agent</label>
  </div><hr/>

### 10. Which of the following metals produce dihydrogen from acids even though they could not produce it by reaction with steam:
  <div className="tb-mcq">
    <input name="question10" type="radio" />
    <label>1. Mg</label><br />
    <input name="question10" type="radio" />
    <label>2. Fe</label><br />
    <input id="q10" name="question10" type="radio" />
    <label htmlFor="q10">3. Cd</label> <br />
    <input name="question10" type="radio" />
    <label>4. Ca</label>
  </div><hr/>

### 11. Methyl orange is a:
  <div className="tb-mcq">
    <input id="q11" name="question11" type="radio" />
    <label htmlFor="q11">1. Weak base</label><br />
    <input name="question11" type="radio" />
    <label>2. Weak acid</label><br />
    <input name="question11" type="radio" />
    <label>3. Amphiprotic ion</label> <br />
    <input name="question11" type="radio" />
    <label>4. Strong acid</label>
  </div><hr/>

### 12. Sodium hydroxide and potassium permanganate are examples of:
  <div className="tb-mcq">
    <input name="question12" type="radio" />
    <label>1. Tertiary standard solution</label><br />
    <input name="question12" type="radio" />
    <label>2. They do not form standard solution</label><br />
    <input name="question12" type="radio" />
    <label>3. Primary standard solution</label> <br />
    <input id="q12" name="question12" type="radio" />
    <label htmlFor="q12">4. Secondary standard solution</label>
  </div><hr/>

### 13. Iodide ion can be oxidized to iodine by:
  <div className="tb-mcq">
    <input name="question13" type="radio" />
    <label>1. Hypo solution</label><br />
    <input id="q13" name="question13" type="radio" />
    <label htmlFor="q13">2. CuSO₄</label><br />
    <input name="question13" type="radio" />
    <label>3. Alkaline KMnO₄</label> <br />
    <input name="question13" type="radio" />
    <label>4. KCl</label>
  </div><hr/>

### 14. Iron on reaction with steam forms:
  <div className="tb-mcq">
    <input id="q14" name="question14" type="radio" />
    <label htmlFor="q14">1. Fe₂O₃</label><br />
    <input name="question14" type="radio" />
    <label>2. Fe₃O₄</label><br />
    <input name="question14" type="radio" />
    <label>3. FeO₄⁻²</label> <br />
    <input name="question14" type="radio" />
    <label>4. FeO</label>
  </div><hr/>

### 15. Choose the correct statement:
  <div className="tb-mcq">
    <input name="question15" type="radio" />
    <label>1. All alkali metals except Li will displace hydrogen from cold water.</label><br />
    <input name="question15" type="radio" />
    <label>2. All decomposition reactions are redox reactions.</label><br />
    <input name="question15" type="radio" />
    <label>3. Rhombic sulphur ongoing disproportionation forms sulphur in -2 and +3 oxidation state.</label> <br />
    <input id="q15" name="question15" type="radio" />
    <label htmlFor="q15">4. There is no way to convert F⁻ to F₂ by chemical means.</label>
  </div><hr/>

### 16. In the reaction between metallic zinc and the copper nitrate solution, using H₂S (a very sensitive test) gas, Cu⁺²:
  <div className="tb-mcq">
    <input name="question16" type="radio" />
    <label>1. Can be detected satisfactorily</label><br />
    <input name="question16" type="radio" />
    <label>2. Depend on concentration </label><br />
    <input id="q16" name="question16" type="radio" />
    <label htmlFor="q16">3. Cannot be detected</label> <br />
    <input name="question16" type="radio" />
    <label>4. Give black precipitate on reacting</label>
  </div><hr/>

### 17. Rate of hydrogen gas evolution is fastest for:
  <div className="tb-mcq">
    <input name="question17" type="radio" />
    <label>1. Na</label><br />
    <input id="q17" name="question17" type="radio" />
    <label htmlFor="q17">2. Mg</label><br />
    <input name="question17" type="radio" />
    <label>3. Ca</label> <br />
    <input name="question17" type="radio" />
    <label>4. Fe</label>
  </div><hr/>

### 18. Thiosulphate on reaction with bromine liquid forms:
  <div className="tb-mcq">
    <input id="q18" name="question18" type="radio" />
    <label htmlFor="q18">1. SO₄⁻²</label><br />
    <input name="question18" type="radio" />
    <label>2. S₄O₆⁻²</label><br />
    <input name="question18" type="radio" />
    <label>3. S₂O₈⁻²</label> <br />
    <input name="question18" type="radio" />
    <label>4. SO₃⁻²</label>
  </div>